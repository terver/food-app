var express = require('express');
var router = express.Router();
const CategoriesController = require("../controllers/category"); 

//Classroom Controllers
router.get("/", CategoriesController.list);
router.get("/:id", CategoriesController.getById);
router.post("/", CategoriesController.add);
router.put("/:id", CategoriesController.update);
router.delete("/:id", CategoriesController.delete);
router.post('/add_with_food', CategoriesController.addWithFooditems);


module.exports = router;
