const express = require('express');
const router = express.Router();
const FoodController = require("../controllers/order");

//Order Controllers
router.get("/", FoodController.list);
router.get("/:id", FoodController.getById);
router.post("/", FoodController.add);
router.patch("/:id", FoodController.update);
router.delete("/:id", FoodController.delete); 
module.exports = router;