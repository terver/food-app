var express = require('express');
var router = express.Router();
const FoodController = require("../controllers/food");

//Classroom Controllers
router.get("/", FoodController.list);
router.get("/:id", FoodController.getById);
router.post("/", FoodController.add);
router.put("/:id", FoodController.update);
router.delete("/:id", FoodController.delete); 

module.exports = router;
