const category = require('./category'); 
const food = require('./food'); 
const order = require('./order'); 
const jumps = require('./jumpps'); 
module.exports = {
    category, 
    food,
    order,
    jumps
};