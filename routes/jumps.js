"use strict";
const express = require('express');
const router = express.Router(); 

 
router.post("/", (req, res, next) => { 
    
    let jumps = req.body.jumps
    let heights = req.body.heights;
    let uniqueHeights;
    let neededFloors = '';
    let heightsCount = heights.length
      
    Array.prototype.unique = function () {
        return this.filter(function (value, index, self) {
            return self.indexOf(value) === index;
        });
    }
 
    uniqueHeights = heights.unique();
    sortedHeights = heights.sort((a, b) => a - b);

   for (let h = 0; h < uniqueHeights.length; h++) {
       const height = uniqueHeights[h];

       let search = height

       let count = heights.reduce(function (n, val) {
           return n + (val === search);
       }, 0);

       if (count >= jumps) {
           neededFloors = 0; 
           break;
       };
       
   } 
   
   if(neededFloors == ''){
       
       
       let heightsList = [];
       for (let h = 0; h < sortedHeights.length; h++) {
          
        if (h + 1 < jumps) {
            continue;
        }

           const height = sortedHeights[h];
        //    console.log(height);
          // additionalHeights.push(height);
           let additionalHeights = [];
           
           for (let i = 1; i < jumps; i++) { 

               let diff = height - sortedHeights[h - i];
            //    console.log(diff)
               additionalHeights.push(diff);

           } 
           let sum = arr => arr.reduce((a, b) => a + b, 0);
           heightsList.push(sum(additionalHeights)); 

           Array.prototype.min = function () {
               return Math.min.apply(null, this);
           };
           neededFloors = heightsList.min();
       }       
       
   }
 
   

    res.status(200).send({
        "jumps": req.body.jumps,
        "heightsNumber": heightsCount,
        "NeededFloors": neededFloors
          });
}); 

 
module.exports = router;
