const Category = require('../models').Category;
const Food = require('../models').Food;
// const Sequelize = require("sequelize");
module.exports = {
    list(req, res) {
        // Category.sync({ force: true });
        return Category 
            .all()
            .then((categories) =>{
                
                res.status(200).send(categories)
            } )
            .catch((error) => { res.status(400).send(error); }); 
    },
    getById(req, res) {
        return Category.findByPk(req.params.id )
            .then(category => {
                if (!category) {
                    return res.status(404).send({
                        message: "category Not Found"
                    });
                }
                return res.status(200).send(category);
            })
            .catch(error => res.status(400).send(error));
    },

    add(req, res) {
        return Category
            .create({
                name: req.body.name,
            })
            .then((category) => res.status(201).send(category))
            .catch((error) => res.status(400).send(error));
    },
    update(req, res) {
        return Category
            .findById(req.params.id/* , {
                include: [{
                    model: Student,
                    as: 'students'
                }],
            } */)
            .then(category => {
                if (!category) {
                    return res.status(404).send({
                        message: 'category Not Found',
                    });
                }
                return category
                    .update({
                        name: req.body.name || category.name,
                    })
                    .then(() => res.status(200).send(category))
                    .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },
    addWithFooditems(req, res) { 
        return Category
            .create({
                class_name: req.body.name,
            }
                )
            .then((classroom) => res.status(201)
           .send(classroom))
            .catch((error) => res.status(400).send(error));
    },
    delete(req, res) {
         Category
            .findById(req.params.id)
            .then(category => {
                if (!category) {
                    return res.status(400).send({
                        message: 'category Not Found',
                    });
                }
                category
                    .destroy()
                    .then(() =>{
                        return res.status(204).json({ message: 'category Deleted' });
                    })
                    .catch((error) => res.status(400).send(error));

            })
            .catch((error) => res.status(400).send(error));
    },

  
};