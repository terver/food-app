const Order = require('../models').Order;
const OrderItem = require('../models').OrderItem;
const Food = require('../models').Order;
const db = require("../models/index");
const sequelize = db.sequelize;
/*  OrderItem.sync({ force: true });
Order.sync({ force: true }); */
module.exports = {
    list(req, res) {
        let totalOrders = [];
        return Order
            .findAll()
            .map(async (order) => {
                let orderInfo = order; 
                orderDetails = await OrderItem.findAll({
                    where: {
                        orderId: order.id
                    }
                });
                totalOrders.push({orderInfo, orderDetails});
                // console.log(orderDetails)
            })
            .then(()=> {
                res.status(200).send(totalOrders)
            })
            .catch((error) => { res.status(400).send(error); });

        // OrderItem.findAll().then(resp => res.status(200).send(resp))
    },
    getById(req, res) {
        return Order.findByPk(req.params.id)
            .then(order => {
                if (!order) {
                    return res.status(404).send({
                        message: "order Not Found"
                    });
                }
                return res.status(200).send(order);
            })
            .catch(error => res.status(400).send(error));
    },

    add(req, res) { 
        let outputOrderObject = {};
        let orderItemArray = [];
        return sequelize.transaction(function (t) {
        
        return Order
            .create({
                customerName: req.body.name,
                address: req.body.address,
                transId: req.body.transId             
            }, { transaction: t })
            .then(async (order) => {
                let orderItems = [] = req.body.orderItems;
                
                outputOrderObject.order = order;

                let i = 1;
                await orderItems.map(  (orderItem) => {
                i = i++;
                if (i > 1) {
                    console.log('THIS SHOULD FAIL');
                    foodItems =   OrderItem.create({
                        foodName: orderItem.foodName,
                      VSVV: orderItem.price,
                        ordd: order.id,
                        quantity: orderItem.quantity,
                    }, { transaction: t });
                }else{
                    foodItems =   OrderItem.create({
                        foodName: orderItem.foodName,
                        price: orderItem.price,
                        orderId: order.id,
                        quantity: orderItem.quantity,
                    }, { transaction: t });
                }
                    // outputOrderObject.foodItems = foodItems;
                    // outputOrderItems.push(outputOrderObject);
                    orderItemArray.push(foodItems);

                });

                outputOrderObject.orderItemArray = orderItemArray;               
            })
            
        })
            .then(() => {
                res.status(200).send(outputOrderObject);
            })
            .catch((error) => res.status(400).send(error));
    },
    update(req, res) {
        return Order
            .findById(req.params.id)
            .then(order => {
                if (!order) { return res.status(404).send({
                        message: 'order Not Found',
                    });
                }

            return sequelize.transaction(function (t) {
            
                let outputOrderObject = {}; 
                return order
                    .update({ 
                        customerName: req.body.name || order.name,
                        address: req.body.address || order.address         
                    }, { transaction: t })
                    .then( (order) => {
                         outputOrderObject.order = order;

                      OrderItem.destroy({ where: 
                            { orderId: order.id }
                        }, { transaction: t });
                    })
                        .then(()=>{
                            let orderItems = [] = req.body.orderItems;
                           return OrderItem.bulkCreate(orderItems, { transaction: t })
                        })

                        .then(()=>  OrderItem.findAll({ where:{  orderId: outputOrderObject.order.id }}))
                        .then(updatedOrderItems =>{

                            outputOrderObject.orderItems = updatedOrderItems;
                            return  res.status(200).send(outputOrderObject);
                        })  
                        .catch((error) => res.status(400).send(error));
                    })
                 
                .catch((error) => res.status(400).send(error));
            })
            .catch((error) => res.status(400).send(error));
    },

    delete(req, res) {

        return Order
            .findById(req.params.id)
            .then(order => {
                if (!order) {
                    return res.status(400).send({
                        message: 'order Not Found',
                    });
                }
                return order
                    .destroy()
                    .then(() => res.status(204).json({
                        message: 'order Deleted',
                    })
                    )
                    .catch((error) => res.status(400).send(error));

            })
            .catch((error) => res.status(400).send(error));
    },


};