const Category = require('../models').Category;
const Food = require('../models').Food;
// const Sequelize = require("sequelize");
// Food.sync({ force: true });
module.exports = {
    list(req, res) {

        
        return Food
            .all( ) 
            .then((food) => res.status(200).send(food))
            .catch((error) => { res.status(400).send(error); });
    },
    getById(req, res) {
        return Food.findByPk(req.params.id)
            .then(food => {
                if (!food) {
                    return res.status(404).send({
                        message: "food Not Found"
                    });
                }
                return res.status(200).send(food);
            })
            .catch(error => res.status(400).send(error));
    },

    add(req, res) {
        //console.log(req.body.price);
        return Food 
            .create({
                name: req.body.name,
                price: req.body.price,
                categoryId: req.body.category
            })
            .then((food) => res.status(201).send(food))
            .catch((error) => res.status(400).send(error));
    },
    update(req, res) {
        return Food
            .findById(req.params.id )
            .then(food => {
                if (!food) {
                    return res.status(404).send({
                        message: 'food Not Found',
                    });
                }
        return food
                .update({
                    name: req.body.name || food.name,
                    price: req.body.price || food.price
                })
                .then(() => res.status(200).send(food))
                .catch((error) => res.status(400).send(error));
        })
        .catch((error) => res.status(400).send(error));
    },
    
    delete(req, res) {
         
        return Food
            .findById(req.params.id)
            .then(food => {
                if (!food) {
                    return res.status(400).send({
                        message: 'food Not Found',
                    });
                }
                return food
                    .destroy()
                    .then(() => res.status(204).json({
                        message: 'food Deleted',
                    })
                    )
                    .catch((error) => res.status(400).send(error));

            })
            .catch((error) => res.status(400).send(error));
    },


};