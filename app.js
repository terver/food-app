const express = require('express');
const morgan = require("morgan");
const bodyParser = require('body-parser'); 

const categoryRoutes = require('./routes/category'); 
const foodRoutes = require('./routes/food'); 
const orderRoutes = require('./routes/order'); 
const jumpsRoutes = require('./routes/jumps'); 

const app = express();

// app.use((req, res, next) => {

//     res.status(200).json({
//         message: 'it is working'
//     });
// });

 

app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );

    if (req.method == 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
        return res.status(200).json({});
    }

    next();
});

// Use routes
app.use('/food', foodRoutes);
app.use('/category', categoryRoutes);
app.use('/order', orderRoutes);
app.use('/jumps', jumpsRoutes);

app.use((req, res, next) => {
    const error = new Error('Not Found here');

    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {

    res.status(error.status || 500).json({
        message: error.message
    });

});

module.exports = app;