const db = require('sequelize');
'use strict';
 module.exports = (sequelize, DataTypes) => {
  const Order = sequelize.define('Order', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    customerName: {
      type: DataTypes.STRING
    },
    address: {
      type: DataTypes.STRING
    },
    transId: {
      type: DataTypes.STRING
    } 

  }, {});
  Order.associate = function(models) {
    // associations can be defined here
  };
  return Order
 };
 