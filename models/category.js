'use strict';
module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define('Category', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: { 
      type: DataTypes.STRING
    },
    
  }, {});
  Category.associate = function(models) {
  //   // associations can be defined here
  //    Category.hasMany(models.Food, {
  //      foreignKey: 'categoryId',
  //      as: 'foods'
  //    });
  };
  return Category;
};