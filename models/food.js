'use strict';
module.exports = (sequelize, DataTypes) => {
  const Food = sequelize.define('Food', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: {
      type: DataTypes.STRING,
    },
    price: {
      type: DataTypes.INTEGER
    },
    categoryId: {
      type: DataTypes.INTEGER
    },
  }, {});


  Food.associate = function(models) {
   /*  Food.belongsTo(models.Category,{
      foreignKey: 'categoryId',
      onDelete: 'CASCADE'
    }); */
  };
  return Food;
};