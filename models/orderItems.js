'use strict';
module.exports = (sequelize, DataTypes) => {
    const OrderItem = sequelize.define('OrderItem', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER
        },
        orderId: {
            type: DataTypes.INTEGER
        },
        foodName: {
            type: DataTypes.STRING
        },
        price: {
            type: DataTypes.DOUBLE
        },
        quantity: {
            type: DataTypes.INTEGER
        }

    }, {});
    OrderItem.associate = function (models) {
        // associations can be defined here
    };
    return OrderItem
};
